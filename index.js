const express = require('express');
const app = new express();
const path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('postgres://usuario:password@host:puerto/nombrebd');

const Employee = sequelize.define('Employee', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
  tableName: 'Employees'
});

// Employee.sync(); -- Si no existe la tabla en la BD, la crea
// Employee.sync({ force: true }); -- Si existe la tabla, la elimina y la vuelve a crear
// Employee.sync({ alter: true }); //Actualiza la tabla de acuerdo a la definicion del modelo

app.get('/empleados', async (req, res) => {
  try {
    const r = await Employee.findAll();
    res.status(200).json(r);
  } catch (error) {
    console.error(error);
  }

});

app.listen(3000, ()=>{
  console.log('Iniciado');
});
